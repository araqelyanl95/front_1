import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor() {}
  setToken(token: string): void {
    localStorage.setItem('assetminder_token', String(token));
  }

  getToken(): any | null {
    return localStorage.getItem('assetminder_token');
  }
}
