import { Injectable } from '@angular/core';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { CreateJobComponent } from '../shared/dialog/job/create-job/create-job.component';
import { JobInfoComponent } from '../shared/dialog/job/job-info/job-info.component';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(private matDialog: MatDialog) {}

  openJobInfo(data?: any): MatDialogRef<JobInfoComponent> {
    return this.matDialog.open(JobInfoComponent, { data, width: '95%', disableClose: true });
  }

  createNewJob(): MatDialogRef<CreateJobComponent> {
    return this.matDialog.open(CreateJobComponent, { width: '60%', disableClose: true });
  }
}
