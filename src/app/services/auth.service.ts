import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import userList from 'src/backend/users.json';
import { Observable } from 'rxjs';
import { ThemeService } from '../theme/theme.service';
import { Router } from '@angular/router';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  apiURL = environment.apiURL;
  users = userList;
  constructor(private themeService: ThemeService, private router: Router, private tokenService: TokenService) {}

  login(email: string, password: string): any {
    const activeUser = this.users.find((user) => user.email === email && user.password === password);
    if (activeUser) {
      this.tokenService.setToken(activeUser.token);
      const theme = activeUser.theme;
      this.themeService.setTheme(theme);
      this.router.navigate(['/job-cards']);
    }
  }
}
