import { Injectable, Inject, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Theme, THEMES } from './models';

@Injectable()
export class ThemeService {
  activeThemeBS: BehaviorSubject<any> = new BehaviorSubject(null);
  activeTheme = this.activeThemeBS.asObservable();

  constructor(@Inject(THEMES) public themes: Theme[]) {
    if (this.getTheme()) {
      this.setTheme(this.getTheme());
    } else {
      this.setTheme('template_1');
    }
  }

  getTheme(): any | null {
    return localStorage.getItem('assetminder_theme');
  }

  setTheme(name: string) {
    this.activeThemeBS.next(this.getProperties(name));
    localStorage.setItem('assetminder_theme', name);
  }

  getProperties(name: string) {
    return this.themes.find((x) => x.name === name)?.properties;
  }
}
