import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { JobInfoComponent } from './job/job-info/job-info.component';
import { CreateJobComponent } from './job/create-job/create-job.component';
@NgModule({
  declarations: [JobInfoComponent, CreateJobComponent, CreateJobComponent],
  imports: [CommonModule, TranslateModule],
})
export class DialogModule {}
