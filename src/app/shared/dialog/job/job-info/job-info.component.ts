import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ThemeService } from 'src/app/theme/theme.service';

@Component({
  selector: 'app-job-info',
  templateUrl: './job-info.component.html',
  styleUrls: ['./job-info.component.scss'],
})
export class JobInfoComponent implements OnInit {
  styleProps: any;
  constructor(private dialogRef: MatDialogRef<JobInfoComponent>, private themeService: ThemeService) {}

  ngOnInit(): void {
    this.themeService.activeTheme.subscribe((res) => {
      this.styleProps = res;
    });
  }

  cancel() {
    this.dialogRef.close('close');
  }
}
