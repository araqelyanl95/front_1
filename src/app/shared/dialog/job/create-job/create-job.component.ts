import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ThemeService } from 'src/app/theme/theme.service';

@Component({
  selector: 'app-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.scss'],
})
export class CreateJobComponent implements OnInit {
  styleProps: any;
  constructor(private dialogRef: MatDialogRef<CreateJobComponent>, private themeService: ThemeService) {}

  ngOnInit(): void {
    this.themeService.activeTheme.subscribe((res) => {
      this.styleProps = res;
    });
  }

  close() {
    this.dialogRef.close('close');
  }
}
