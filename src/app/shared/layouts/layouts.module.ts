import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeftNavbarComponent } from './left-navbar/left-navbar.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { TranslateModule } from '@ngx-translate/core';
import { RightNavbarComponent } from './right-navbar/right-navbar.component';

@NgModule({
  declarations: [LeftNavbarComponent, HeaderComponent, RightNavbarComponent],
  imports: [CommonModule, RouterModule, TranslateModule],
  exports: [LeftNavbarComponent, HeaderComponent, RightNavbarComponent],
})
export class LayoutsModule {}
