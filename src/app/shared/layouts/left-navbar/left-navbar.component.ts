import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-left-navbar',
  templateUrl: './left-navbar.component.html',
  styleUrls: ['./left-navbar.component.scss'],
})
export class LeftNavbarComponent implements OnInit {
  constructor() {}

  menu: any = [
    { name: 'Suppliers', link: 'suppliers', icon: 'assets/images/icons/supplier.png' },
    { name: 'Customers', link: 'customers', icon: 'assets/images/icons/customers.png' },
    { name: 'Stock', link: 'stock', icon: 'assets/images/icons/stock.png' },
    { name: 'Purchasing', link: 'purchasing', icon: 'assets/images/icons/purchasing.png' },
    { name: 'Assets', link: 'assets', icon: 'assets/images/icons/assets.png' },
    {
      name: 'Job Cards',
      link: 'job-cards',
      icon: 'assets/images/icons/job_cards.png',
      submenus: { name: 'Edit Booking', link: 'edit_booking' },
    },
    { name: 'Leasing', link: 'leasing', icon: 'assets/images/icons/leasing.png' },
    { name: 'Reports', link: 'reports', icon: 'assets/images/icons/report.png' },
    { name: 'Inspections', link: 'inspections', icon: 'assets/images/icons/inspections.png' },
    { name: 'Drivers', link: 'drivers', icon: 'assets/images/icons/driver.png' },
    { name: 'Other', link: 'other', icon: 'assets/images/icons/other.png' },
  ];

  ngOnInit(): void {}
}
