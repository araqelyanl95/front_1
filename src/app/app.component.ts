import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThemeService } from './theme/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'fleetminder-web-angular';
  styleProps: any;
  constructor(private translate: TranslateService, private themeService: ThemeService) {
    translate.setDefaultLang('en');
    this.themeService.activeTheme.subscribe((res) => {
      this.styleProps = res;
    });
  }
}
