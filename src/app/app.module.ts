import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// translate
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AuthModule } from './auth/auth.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogModule } from './shared/dialog/dialog.module';
import { ThemeService } from './theme/theme.service';
import { ThemeModule } from './theme/theme.module';
import { template_1 } from './theme/templates/template-1';
import { template_3 } from './theme/templates/template-3';
import { template_4 } from './theme/templates/template-4';
import { template_5 } from './theme/templates/template-5';
import { template_2 } from './theme/templates/template-2';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    AuthModule,
    BrowserAnimationsModule,
    MatDialogModule,
    DialogModule,
    ThemeModule.forRoot({
      themes: [template_1, template_2, template_3, template_4, template_5],
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
