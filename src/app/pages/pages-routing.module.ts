import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobInfoComponent } from './job-cards/job-info/job-info.component';
import { EditBookingComponent } from './edit-booking/edit-booking.component';
import { JobCardsComponent } from './job-cards/job-cards.component';
import { PagesComponent } from './pages.component';
import { StockComponent } from './stock/stock.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: 'job-cards', component: JobCardsComponent },
      { path: 'job-card/:id', component: JobInfoComponent },
      { path: 'stock', component: StockComponent },
      { path: 'edit-booking', component: EditBookingComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
