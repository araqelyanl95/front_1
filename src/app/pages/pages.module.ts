import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { LayoutsModule } from '../shared/layouts/layouts.module';
import { JobCardsComponent } from './job-cards/job-cards.component';
import { JobInfoComponent } from './job-cards/job-info/job-info.component';
import { TranslateModule } from '@ngx-translate/core';
import { StockComponent } from './stock/stock.component';
import { EditBookingComponent } from './edit-booking/edit-booking.component';

// Material
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ThemeService } from '../theme/theme.service';

@NgModule({
  declarations: [PagesComponent, JobCardsComponent, StockComponent, EditBookingComponent, JobInfoComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    LayoutsModule,
    TranslateModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
  ],
  providers: [ThemeService],
})
export class PagesModule {}
