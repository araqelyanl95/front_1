import { Component, OnInit } from '@angular/core';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-job-info',
  templateUrl: './job-info.component.html',
  styleUrls: ['./job-info.component.scss'],
})
export class JobInfoComponent implements OnInit {
  constructor(private dialogService: DialogService) {}

  ngOnInit(): void {}

  openDialogInfo() {
    this.dialogService
      .openJobInfo()
      .afterClosed()
      .subscribe((res) => {});
  }
}
