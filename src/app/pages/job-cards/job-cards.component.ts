import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogService } from 'src/app/services/dialog.service';
import jobs from 'src/backend/job-cards.json';

@Component({
  selector: 'app-job-cards',
  templateUrl: './job-cards.component.html',
  styleUrls: ['./job-cards.component.scss'],
})
export class JobCardsComponent implements OnInit {
  constructor(private dialogService: DialogService, private fb: FormBuilder, private router: Router) {
    this.getJobCards();
  }
  jobCards = jobs;
  openJobs: any[] = [];
  pendingJobs: any[] = [];
  closedJobs: any[] = [];

  jobTypes: any[] = [
    'Asset registration number',
    'Job number',
    'Vin/Serial',
    'Invoice number',
    'Part number',
    'Fleet number',
    'Owner',
    'Operator',
    'Invoice',
  ];
  locations: any[] = ['Armour', 'Dublin', 'Galway', 'Limerick'];
  status: any[] = ['Open', 'Active', 'Inactive'];
  vmu: any[] = ['Abc', 'deleted', 'minsk', 'vmu1'];

  filterForm: FormGroup = this.fb.group({
    type: 'label',
    location: [],
    status: [],
    vmu: [],
  });

  ngOnInit(): void {}

  getJobCards() {
    this.openJobs = this.jobCards.filter((job) => job.type === 'open');
    this.pendingJobs = this.jobCards.filter((job) => job.type === 'pending');
    this.closedJobs = this.jobCards.filter((job) => job.type === 'closed');
  }

  createNewJob() {
    this.dialogService
      .createNewJob()
      .afterClosed()
      .subscribe((res) => {});
  }

  selectAllLocations(event: any) {
    if (event.checked) {
      this.filterForm.controls.location.patchValue(this.locations);
    } else {
      this.filterForm.controls.location.patchValue([]);
    }
  }

  selectAllStatus(event: any) {
    if (event.checked) {
      this.filterForm.controls.status.patchValue(this.status);
    } else {
      this.filterForm.controls.status.patchValue([]);
    }
  }

  selectAllVMU(event: any) {
    if (event.checked) {
      this.filterForm.controls.vmu.patchValue(this.vmu);
    } else {
      this.filterForm.controls.vmu.patchValue([]);
    }
  }

  openJobInfo(jobNumber: number) {
    this.router.navigate(['/job-card/' + jobNumber]);
  }
}
